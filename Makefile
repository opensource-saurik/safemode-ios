TARGET := iphone:7.1:2.0
ARCHS := armv6 armv7s arm64
PACKAGE_VERSION := $(shell ./version.sh)

include theos/makefiles/common.mk

TWEAK_NAME := MobileSafety
MobileSafety_FILES := Tweak.xm
MobileSafety_FRAMEWORKS := UIKit

ADDITIONAL_CFLAGS += -mllvm -arm-reserve-r9
ADDITIONAL_LDFLAGS += -Xarch_armv6 -Wl,-lgcc_s.1

ADDITIONAL_LDFLAGS += -Xarch_armv6 -Wl,-segalign,4000
ADDITIONAL_LDFLAGS += -Xarch_armv7s -Wl,-segalign,4000

include $(THEOS_MAKE_PATH)/tweak.mk
